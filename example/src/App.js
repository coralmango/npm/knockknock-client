import React from 'react'

import { LoginForm } from '@coralmango/rails-auth-client'
import 'antd/dist/antd.css'
import '@coralmango/rails-auth-client/dist/index.css'

const App = () => {
  return <LoginForm />
}

export default App
