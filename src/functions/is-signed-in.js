import { LOCALSTORAGE_TOKEN_KEY } from '../constants'

const isLoggedIn = () => {
  const authToken = window.localStorage.getItem(LOCALSTORAGE_TOKEN_KEY)
  if (authToken) return true
  return false
}
export default isLoggedIn
