import { LOCALSTORAGE_TOKEN_KEY } from '../constants'

const signIn = (jwt) => {
  window.localStorage.setItem(LOCALSTORAGE_TOKEN_KEY, JSON.stringify(jwt))
}

export default signIn
