import { LOCALSTORAGE_TOKEN_KEY } from '../constants'

const signOut = () => {
  window.localStorage.removeItem(LOCALSTORAGE_TOKEN_KEY)
}
export default signOut
