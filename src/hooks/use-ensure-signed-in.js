import { useEffect } from 'react'
import isLoggedIn from '../functions/is-signed-in'

const useEnsureLoggedIn = (router, loginPath = '/auth/login') => {
  useEffect(() => {
    if (!isLoggedIn()) router.push(loginPath)
  }, [])
}

export default useEnsureLoggedIn
