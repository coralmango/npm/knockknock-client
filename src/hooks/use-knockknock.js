import { useEffect, useState } from 'react'
import rawSignOut from '../functions/sign-out'

const useKnockknock = (router, loginPath = '/auth/login') => {
  const [shouldRedirectToSignIn, setShouldRedirectToSignIn] = useState(false)

  useEffect(() => {
    if (!shouldRedirectToSignIn) return
    rawSignOut()
    router.push(loginPath)
  }, [shouldRedirectToSignIn])

  const signOut = () => {
    setShouldRedirectToSignIn(true)
  }

  return { signOut }
}

export default useKnockknock
