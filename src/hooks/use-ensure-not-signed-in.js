import { useEffect } from 'react'
import isLoggedIn from '../functions/is-signed-in'

const useEnsureNotLoggedIn = (router, loggedInRootPath = '/') => {
  useEffect(() => {
    if (isLoggedIn()) router.push(loggedInRootPath)
  }, [])
}

export default useEnsureNotLoggedIn
