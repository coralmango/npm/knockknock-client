import React, { useState } from 'react'
import { Input, Button, Form } from 'antd'
import { LockOutlined, MailOutlined } from '@ant-design/icons'

import styles from './login-form.module.css'
import signIn from '../functions/sign-in'

const SocialLogin = () => null

const LoginForm = ({
  router,
  heading,
  onSubmit,
  onForgotPasswordClick,
  forgotPasswordPath = '/auth/forgot-password',
  onRegisterClick,
  registerPath = '/auth/register',
  configuredApiClient,
  disableRegistration = false,
  disableForgotPassword = false,
  onAfterLogin,
  afterLoginPath
}) => {
  const [submitting, setSubmitting] = useState(false)

  const buttonLabel = submitting ? ' Logging in...' : 'Login'

  const handleRegisterClick = async () => {
    if (disableRegistration) return
    if (onRegisterClick) return await onRegisterClick()
    if (router && registerPath) return router.push(registerPath)
  }

  const handleForgotPasswordClick = async () => {
    if (disableForgotPassword) return
    if (onForgotPasswordClick) return await onForgotPasswordClick()
    if (router && forgotPasswordPath) return router.push(forgotPasswordPath)
  }

  const handleAfterLogin = async (values, response) => {
    // TODO: Handle error cases

    if (response?.data?.jwt) {
      signIn(response.data.jwt)
    }

    if (onSubmit) {
      await onSubmit(values)
      setSubmitting(false)
      return
    }
    if (onAfterLogin) {
      await onAfterLogin(values)
      setSubmitting(false)
      return
    }
    if (router && afterLoginPath) router.push(afterLoginPath)
  }

  const handleFinish = async (values) => {
    setSubmitting(true)

    let response

    if (configuredApiClient) {
      response = await configuredApiClient.fetch({
        apiEndpoint: { url: '/api/users/login', method: 'post' },
        apiData: { auth: values }
      })
    }

    if (response) {
      await handleAfterLogin(values, response)
    }
  }

  return (
    <div className={styles.header}>
      {heading && <h1>{heading}</h1>}
      <Form onFinish={handleFinish} className='login-form form-card'>
        <Form.Item
          name='email'
          rules={[{ required: true, message: 'Please enter email!' }]}
        >
          <Input
            prefix={<MailOutlined />}
            placeholder='Email'
            autoComplete='username'
          />
        </Form.Item>
        <Form.Item
          name='password'
          rules={[{ required: true, message: 'Please enter password!' }]}
        >
          <Input.Password
            type='password'
            autoComplete='current-password'
            prefix={<LockOutlined />}
            placeholder='Password'
          />
        </Form.Item>
        <Button
          type='primary'
          block
          htmlType='submit'
          className='login-button'
          loading={submitting}
        >
          {buttonLabel}
        </Button>
        {process.env.REACT_APP_FIREBASE_API_KEY ? <SocialLogin /> : null}

        {!disableForgotPassword && (
          <p style={{ margin: '10px 0px' }}>
            <Button
              type='link'
              style={{ padding: 0 }}
              onClick={handleForgotPasswordClick}
            >
              Forgot Password
            </Button>
          </p>
        )}

        {!disableRegistration && (
          <p className='switch-auth-link' style={{ marginBottom: 0 }}>
            Don't have an account?{' '}
            <Button
              type='link'
              style={{ padding: 0 }}
              onClick={handleRegisterClick}
            >
              Register
            </Button>
          </p>
        )}
      </Form>
    </div>
  )
}

export default LoginForm
