# @coralmango/rails-auth-client

> Auth Client for Rails apps developed at CoralMango

[![NPM](https://img.shields.io/npm/v/@coralmango/rails-auth-client.svg)](https://www.npmjs.com/package/@coralmango/rails-auth-client) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @coralmango/rails-auth-client
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from '@coralmango/rails-auth-client'
import '@coralmango/rails-auth-client/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [dineshgadge](https://github.com/dineshgadge)
